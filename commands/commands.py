from tools import retrieve_price_from_monitor
from monitors import monitors

def setup_commands(bot):
    @bot.command(description='Gives the price of dollar in relation with Bolivar currency') 
    async def dolar(ctx):
        dolar_today_price = retrieve_price_from_monitor(monitors["dolar_today"])
        dolar_bcv_price = retrieve_price_from_monitor(monitors["dolar_bcv"])
        await ctx.respond(
            ':up: :green_circle: :green_circle: :green_circle: :green_circle: :green_circle: :up: \n\n' +
            f'**DolarToday**: *{dolar_today_price}* <:DolarToday:1172348982215848036> \n' +
            f'**Dolar BCV**: *{dolar_bcv_price}* <:BCV:1172355360716238920> \n\n' +
            ':up: :green_circle: :green_circle: :green_circle: :green_circle: :green_circle: :up:'
        )

    @bot.command(description='Gives SLP poor price') 
    async def slp(ctx):
        SLP_PRICE = retrieve_price_from_monitor(monitors["slp"])
        await ctx.respond(
            ':sob: :sob: :sob: :sob: :sob: :sob: :sob:  \n\n' +
            f'**SLP**: *{SLP_PRICE}* <:SLP:1172358222204641381> \n\n' +
            ':sob: :sob: :sob: :sob: :sob: :sob: :sob: '
        )