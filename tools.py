from bs4 import BeautifulSoup
from operator import itemgetter
import requests
import os

def getHTML(uri):
    url = uri
    HEADERS = {"content-type": "image/png"}
    html = requests.get(url, headers=HEADERS).text
    return BeautifulSoup(html, "html.parser")
    #this function returns the HTML which can be used with soup

def retrieve_price_from_monitor(url):
    soup = getHTML(url)
    dolar_price = soup.select_one('div.texto h2')
    return dolar_price.contents[0]