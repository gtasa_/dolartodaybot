from events.events import setup_events
from commands.commands import setup_commands
import discord
import os
from dotenv import load_dotenv

# Load enviroment variables
load_dotenv()
BOT_TOKEN = os.getenv('BOT_TOKEN')

# Create bot and setup events and commands
bot = discord.Bot()
setup_events(bot)
setup_commands(bot)

# Run the bot with the token loaded from the enviroment variables
bot.run(BOT_TOKEN)